package entity

import (
	"gitlab.com/ugrd/orchestration-saga/account/domain/contract"
	"gorm.io/gorm"
	"time"
)

// User is a struct that represents users table
type User struct {
	ID        uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	Name      string         `gorm:"column:name;size:100;not null;" json:"name"`
	Email     string         `gorm:"column:email;size:100;not null; default:''" json:"email"`
	Username  string         `gorm:"column:username;size:100;not null;uniqueIndex" json:"username"`
	Password  string         `gorm:"column:password;size:255;not null" json:"password"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}

// Implements base entity methods
var _ contract.EntityInterface = &User{}

// TableName is a function return table name
func (u *User) TableName() string {
	return "users"
}

// FilterableFields is a function return filterable fields
func (u *User) FilterableFields() []interface{} {
	return []interface{}{"name", "email", "username"}
}

// TimeFields is a function return time fields
func (u *User) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
