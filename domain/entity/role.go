package entity

import (
	"gitlab.com/ugrd/orchestration-saga/account/domain/contract"
	"time"
)

// Role is a struct that represents roles table
type Role struct {
	ID          uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	Name        string    `gorm:"column:name;size:100;not null;" json:"name"`
	Code        string    `gorm:"column:code;size:100;not null;index; default:''" json:"code"`
	Description string    `gorm:"column:description;type:text;not null;default:''" json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Role{}

// TableName is a function return table name
func (u *Role) TableName() string {
	return "roles"
}

// FilterableFields is a function return filterable fields
func (u *Role) FilterableFields() []interface{} {
	return []interface{}{"name", "code", "description"}
}

// TimeFields is a function return time fields
func (u *Role) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
