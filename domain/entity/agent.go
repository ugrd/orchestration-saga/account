package entity

import (
	"gitlab.com/ugrd/orchestration-saga/account/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Agent is a struct that represents agents table
type Agent struct {
	ID        uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	RoleID    uint           `gorm:"column:role_id; not null;" json:"role_id"`
	Name      string         `gorm:"column:name;size:100;not null;" json:"name"`
	Username  string         `gorm:"column:username;size:100;not null;uniqueIndex" json:"username"`
	Password  string         `gorm:"column:password;size:255;not null" json:"password"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
	Role      Role           `json:"role"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Agent{}

// TableName is a function return table name
func (u *Agent) TableName() string {
	return "agents"
}

// FilterableFields is a function return filterable fields
func (u *Agent) FilterableFields() []interface{} {
	return []interface{}{"name", "username"}
}

// TimeFields is a function return time fields
func (u *Agent) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
