package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
)

// RoleRepoInterface is role repo contract
type RoleRepoInterface interface {
	Create(ctx context.Context, role *entity.Role) error
	Update(ctx context.Context, role *entity.Role, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Role, error)
	FindByCode(ctx context.Context, code string) (*entity.Role, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Role, error)
}
