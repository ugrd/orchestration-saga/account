package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
)

// AgentRepoInterface is agent repo contract
type AgentRepoInterface interface {
	Create(ctx context.Context, agent *entity.Agent) error
	Update(ctx context.Context, agent *entity.Agent, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Agent, error)
	FindByUsername(ctx context.Context, username string) (*entity.Agent, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Agent, error)
}
