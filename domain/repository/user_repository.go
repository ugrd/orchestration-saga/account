package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
)

// UserRepoInterface is user repo contract
type UserRepoInterface interface {
	Create(ctx context.Context, user *entity.User) error
	Update(ctx context.Context, user *entity.User, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.User, error)
	FindByEmail(ctx context.Context, email string) (*entity.User, error)
	FindByUsername(ctx context.Context, username string) (*entity.User, error)
	List(ctx context.Context) ([]*entity.User, error)
}
