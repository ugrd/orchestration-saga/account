package handler_test

import (
	"github.com/jaswdr/faker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/tests"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"testing"
)

func TestHandler_GetUser(t *testing.T) {
	test := tests.Init()

	ctx := test.Ctx
	rep := test.Repo
	hdl := test.Handler

	f := faker.New()

	t.Run("it should return no error, with empty data", func(t *testing.T) {
		r, err := hdl.GetUser(ctx, &account.GetUserRequest{})

		assert.NoError(t, err)
		assert.NotNil(t, r)
		assert.Empty(t, r.Users)
	})

	_ = rep.User.Create(ctx, &entity.User{
		Name:     f.Person().Name(),
		Username: f.RandomStringWithLength(10),
		Password: f.RandomStringWithLength(30),
	})

	t.Run("it should return no error, with 1 row", func(t *testing.T) {
		r, err := hdl.GetUser(ctx, &account.GetUserRequest{})

		assert.NoError(t, err)
		assert.NotNil(t, r)
		assert.NotEmpty(t, r.Users)
		assert.Len(t, r.Users, 1)
	})
}
