package handler_test

import (
	"github.com/jaswdr/faker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/tests"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"testing"
)

func TestHandler_FindUser(t *testing.T) {
	test := tests.Init()

	ctx := test.Ctx
	rep := test.Repo
	hdl := test.Handler

	f := faker.New()

	var (
		id = 100
	)

	t.Run("it should return error, record not found", func(t *testing.T) {
		r, err := hdl.FindUser(ctx, &account.FindUserRequest{Id: uint64(id)})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	_ = rep.User.Create(ctx, &entity.User{
		ID:       uint(id),
		Name:     f.Person().Name(),
		Username: f.RandomStringWithLength(10),
		Password: f.RandomStringWithLength(30),
	})

	t.Run("it should return error, when Name length less then 3", func(t *testing.T) {
		r, err := hdl.FindUser(ctx, &account.FindUserRequest{Id: uint64(id)})

		assert.NoError(t, err)
		assert.NotNil(t, r)
	})
}
