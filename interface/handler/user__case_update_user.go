package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// UpdateUser is a method to update pre-existing user
func (hdl *Handler) UpdateUser(ctx context.Context, req *account.UpdateUserRequest) (*account.User, error) {
	payload := &UserUpdatePayload{
		ID:       req.GetId(),
		Email:    req.GetEmail(),
		Name:     req.GetName(),
		Username: req.GetUsername(),
	}

	errV := payload.Validate()
	if errV != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(errV)).
			ErrList()
	}

	userID := uint(req.GetId())

	_, errF := hdl.Dependency.Repository.User.FindByID(ctx, userID)
	if errF != nil {
		switch errF {
		case gorm.ErrRecordNotFound:
			return nil, status.Error(codes.NotFound, "Data not found")
		default:
			return nil, errF
		}
	}

	existedEmail, _ := hdl.Dependency.Repository.User.FindByEmail(ctx, req.GetEmail())
	if existedEmail != nil && existedEmail.ID == userID {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"email": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	existedUsername, _ := hdl.Dependency.Repository.User.FindByEmail(ctx, req.GetEmail())
	if existedUsername != nil && existedUsername.ID == userID {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"username": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	errUp := hdl.Dependency.Repository.User.Update(ctx, &entity.User{
		Name:     req.GetName(),
		Username: req.GetUsername(),
	}, userID)
	if errUp != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(errUp.Error()).
			Err()
	}

	r, _ := hdl.Dependency.Repository.User.FindByID(ctx, userID)

	return &account.User{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Email:     r.Email,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}

// UserUpdatePayload is a struct
type UserUpdatePayload struct {
	ID       uint64 `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Username string `json:"username"`
}

// Validate is a method to validate
func (u UserUpdatePayload) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ID, validation.Required),
		validation.Field(&u.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(&u.Email, validation.Required, validation.Length(5, 100), is.Email),
		validation.Field(&u.Username, validation.Required, validation.Length(6, 100)),
	)
}
