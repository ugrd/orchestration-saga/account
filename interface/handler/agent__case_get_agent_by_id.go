package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/interface/validator"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// GetAgentByID is a method to retrieve agent by id
func (hdl *Handler) GetAgentByID(ctx context.Context, req *account.IdentifierRequest) (*account.Agent, error) {
	v := &validator.IdentifierID{
		ID: req.GetId(),
	}

	if err := v.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(err)).
			ErrList()
	}

	r, err := hdl.Dependency.Repository.Agent.FindByID(ctx, uint(req.GetId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return &account.Agent{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
		DeletedAt: timestamppb.New(r.DeletedAt.Time),
	}, nil
}
