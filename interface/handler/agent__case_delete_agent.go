package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/interface/validator"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
)

// DeleteAgent is a method to remove existing agent
func (hdl *Handler) DeleteAgent(ctx context.Context, req *account.IdentifierRequest) (*account.DeleteAgentResponse, error) {
	v := &validator.IdentifierID{
		ID: req.GetId(),
	}

	if err := v.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(err)).
			ErrList()
	}

	_, err := hdl.Dependency.Repository.Agent.FindByID(ctx, uint(req.GetId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	err = hdl.Dependency.Repository.Agent.Delete(ctx, uint(req.GetId()))
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &account.DeleteAgentResponse{
		Message: fmt.Sprintf("OK"),
	}, nil
}
