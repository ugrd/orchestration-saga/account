package handler_test

import (
	"fmt"
	"github.com/jaswdr/faker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/account/tests"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/status"
	"regexp"
	"strings"
	"testing"
)

func TestHandler_CreateUser(t *testing.T) {
	test := tests.Init()

	ctx := test.Ctx
	hdl := test.Handler

	f := faker.New()

	t.Run("it should return error, when Name length less then 3", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.RandomStringWithLength(2),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Name length more then 100", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.RandomStringWithLength(101),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Email length more then 100", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.RandomStringWithLength(2),
			Email:    strings.Join([]string{f.RandomStringWithLength(98), "gmail.com"}, "@"),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Username length less then 6", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(5),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Username length more then 100", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(101),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Password length less then 8", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(7),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, when Username length more then 255", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(256),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, existed email", func(t *testing.T) {
		email := f.Internet().Email()

		_, _ = hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    email,
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    email,
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return error, existed username", func(t *testing.T) {
		username := f.RandomStringWithLength(20)

		_, _ = hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: username,
			Password: f.RandomStringWithLength(30),
		})

		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: username,
			Password: f.RandomStringWithLength(30),
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})

	t.Run("it should return no error", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.Person().NameMale(),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		assert.NoError(t, err)
		assert.NotNil(t, r)
	})
}

func TestWithPassword(t *testing.T) {
	test := tests.Init()

	ctx := test.Ctx
	hdl := test.Handler

	f := faker.New()

	t.Run("it should return error, when Name length less then 3", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.RandomStringWithLength(3),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: "Uhjdsjjds*",
		})

		assert.Error(t, err)
		assert.Nil(t, r)
	})
}

func TestUsernameRegex(t *testing.T) {
	pattern := "^[A-Za-z][A-Za-z0-9_]{6,29}$"

	r, err := regexp.Compile(pattern)

	assert.NoError(t, err)

	assert.True(t, r.Match([]byte("mkyong8")))
	assert.True(t, r.Match([]byte("javaregex")))
	assert.True(t, r.Match([]byte("JAVAregex")))
	assert.True(t, r.Match([]byte("java_regex")))
	assert.True(t, r.Match([]byte("java_regex_123")))
	assert.True(t, r.Match([]byte("javaregex123")))
	assert.True(t, r.Match([]byte("L123456")))
	assert.True(t, r.Match([]byte("java123")))
	assert.True(t, r.Match([]byte("M1234567890123456789")))
}

func TestFieldValidation(t *testing.T) {
	test := tests.Init()

	ctx := test.Ctx
	hdl := test.Handler

	f := faker.New()

	t.Run("it should return error, when Name length less then 3", func(t *testing.T) {
		r, err := hdl.CreateUser(ctx, &account.CreateUserRequest{
			Name:     f.RandomStringWithLength(2),
			Email:    f.Internet().Email(),
			Username: f.RandomStringWithLength(10),
			Password: f.RandomStringWithLength(30),
		})

		st := status.Convert(err)
		rs := make(map[string]string)

		for _, detail := range st.Details() {
			switch t := detail.(type) {
			case *errdetails.BadRequest:
				for _, violation := range t.GetFieldViolations() {
					rs[violation.GetField()] = violation.GetDescription()
				}
			}
		}

		fmt.Println(rs)

		assert.Error(t, err)
		assert.Nil(t, r)
	})
}
