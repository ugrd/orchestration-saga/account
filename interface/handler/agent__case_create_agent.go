package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/security"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"regexp"
)

// CreateAgent is a method to create new agent
func (hdl *Handler) CreateAgent(ctx context.Context, req *account.CreateAgentRequest) (*account.Agent, error) {
	payload := &AgentCreatePayload{
		RoleID: req.GetRoleId(),
		Agent: &entity.Agent{
			Name:     req.GetName(),
			Username: req.GetUsername(),
			Password: req.GetPassword(),
		},
	}

	errV := payload.Validate()
	if errV != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(errV)).
			ErrList()
	}

	if !util.IsTruePass(payload.Password) {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"password": "At least one upper case letter, one lower case latter, one digit, one special character",
			}).
			ErrList()
	}

	role, errRole := hdl.Dependency.Repository.Role.FindByID(ctx, uint(req.GetRoleId()))
	if errRole != nil {
		switch errRole {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Role not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errRole.Error()).
				Err()
		}
	}

	_, errUsername := hdl.Dependency.Repository.Agent.FindByUsername(ctx, req.GetUsername())
	if errUsername == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"username": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	newAgent := &entity.Agent{
		RoleID:   role.ID,
		Name:     req.GetName(),
		Username: req.GetUsername(),
		Password: security.HashMake(req.GetPassword()),
	}

	err := hdl.Dependency.Repository.Agent.Create(ctx, newAgent)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(errRole.Error()).
			Err()
	}

	r, _ := hdl.Dependency.Repository.Agent.FindByID(ctx, newAgent.ID)

	return &account.Agent{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
		DeletedAt: timestamppb.New(r.DeletedAt.Time),
	}, nil
}

// AgentCreatePayload is a struct
type AgentCreatePayload struct {
	RoleID uint64 `json:"role_id"`
	*entity.Agent
}

// Validate is a method to validate
func (u AgentCreatePayload) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.RoleID, validation.Required),
		validation.Field(&u.Agent.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(
			&u.Agent.Username,
			validation.Required,
			validation.Length(6, 101),
			validation.Match(regexp.MustCompile("^[A-Za-z][A-Za-z0-9_]{6,29}$")).Error("Must be alpha-numeric, can include underscore"),
		),
		validation.Field(
			&u.Agent.Password,
			validation.Required,
			validation.Length(8, 255),
		),
	)
}
