package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"regexp"
)

// UpdateAgent is a method to update existing agent
func (hdl *Handler) UpdateAgent(ctx context.Context, req *account.UpdateAgentRequest) (*account.Agent, error) {
	payload := &AgentUpdatePayload{
		ID:     req.GetId(),
		RoleID: req.GetRoleId(),
		Agent: &entity.Agent{
			Name:     req.GetName(),
			Username: req.GetUsername(),
		},
	}

	errV := payload.Validate()
	if errV != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(errV)).
			ErrList()
	}

	_, errRole := hdl.Dependency.Repository.Role.FindByID(ctx, uint(req.GetRoleId()))
	if errRole != nil {
		switch errRole {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Role not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errRole.Error()).
				Err()
		}
	}

	_, errH := hdl.Dependency.Repository.Agent.FindByID(ctx, uint(req.GetId()))
	if errH != nil {
		switch errH {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errH.Error()).
				Err()
		}
	}

	resp, errU := hdl.Dependency.Repository.Agent.FindByUsername(ctx, req.GetUsername())
	if errU == nil && resp.ID != uint(req.GetId()) {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"username": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	err := hdl.Dependency.Repository.Agent.Update(ctx, &entity.Agent{
		RoleID:   uint(req.GetRoleId()),
		Name:     req.GetName(),
		Username: req.GetUsername(),
	}, uint(req.GetId()))

	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(errRole.Error()).
			Err()
	}

	r, _ := hdl.Dependency.Repository.Agent.FindByID(ctx, uint(req.GetId()))

	return &account.Agent{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
		DeletedAt: timestamppb.New(r.DeletedAt.Time),
	}, nil
}

// AgentUpdatePayload is a struct
type AgentUpdatePayload struct {
	ID     uint64 `json:"id"`
	RoleID uint64 `json:"role_id"`
	*entity.Agent
}

// Validate is a method to validate
func (u AgentUpdatePayload) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.ID, validation.Required),
		validation.Field(&u.RoleID, validation.Required),
		validation.Field(&u.Agent.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(
			&u.Agent.Username,
			validation.Required,
			validation.Length(7, 101),
			validation.Match(regexp.MustCompile("^[A-Za-z][A-Za-z0-9_]{6,29}$")).Error("Must be alpha-numeric, can include underscore"),
		),
	)
}
