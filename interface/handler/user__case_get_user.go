package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetUser is a method to retrieve users
func (hdl *Handler) GetUser(ctx context.Context, _ *account.GetUserRequest) (*account.Users, error) {
	var users []*account.User

	rows, err := hdl.Dependency.Repository.User.List(ctx)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		users = append(users, &account.User{
			Id:        uint64(row.ID),
			Name:      row.Name,
			Username:  row.Username,
			CreatedAt: timestamppb.New(row.CreatedAt),
			UpdatedAt: timestamppb.New(row.UpdatedAt),
		})
	}

	return &account.Users{
		Users: users,
	}, nil
}
