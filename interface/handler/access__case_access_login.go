package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/constant"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/security"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"strconv"
	"time"
)

// Login is a method
func (hdl *Handler) Login(ctx context.Context, req *account.LoginRequest) (*account.LoginResponse, error) {
	v := &LoginPayload{
		Username: req.GetUsername(),
		Password: req.GetPassword(),
	}

	if err := v.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(err)).
			ErrList()
	}

	r, err := hdl.Dependency.Repository.User.FindByUsername(ctx, req.GetUsername())
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(fmt.Sprintf("Invalid username or password")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	if !security.HashVerify(req.GetPassword(), r.Password) {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unauthenticated).
			WithMsg(fmt.Sprintf("Invalid username or password")).
			Err()
	}

	usr := constant.User
	ttl := time.Hour * 24

	claim := map[string]string{
		entity.XCodeKey: r.Username,
		entity.XID:      strconv.Itoa(int(r.ID)),
		"access":        usr.String(),
	}

	token, err := hdl.Dependency.JWT.Create(ttl, claim)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &account.LoginResponse{
		AccessToken: token,
		ExpiredIn:   time.Now().Add(ttl).Unix(),
	}, nil
}

// LoginPayload is a struct defines login payload
type LoginPayload struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate is function to validate AuthPayload
func (a LoginPayload) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, validation.Required, validation.Length(6, 100)),
		validation.Field(&a.Password, validation.Required, validation.Length(8, 255)),
	)
}
