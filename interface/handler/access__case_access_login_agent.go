package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/security"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"time"
)

// LoginAgent is a method
func (hdl *Handler) LoginAgent(ctx context.Context, req *account.LoginRequest) (*account.LoginResponse, error) {
	v := &LoginPayload{
		Username: req.GetUsername(),
		Password: req.GetPassword(),
	}

	if err := v.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(err)).
			ErrList()
	}

	r, err := hdl.Dependency.Repository.Agent.FindByUsername(ctx, req.GetUsername())
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unauthenticated).
				WithMsg(fmt.Sprintf("Invalid username or password")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	if !security.HashVerify(req.GetPassword(), r.Password) {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unauthenticated).
			WithMsg(fmt.Sprintf("Invalid username or password")).
			Err()
	}

	ttl := time.Hour * 24

	claim := map[string]string{
		"x-code": r.Username,
		"access": r.Role.Code,
	}

	token, err := hdl.Dependency.JWT.Create(ttl, claim)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &account.LoginResponse{
		AccessToken: token,
		ExpiredIn:   time.Now().Add(ttl).Unix(),
	}, nil
}
