package core

import "gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"

// GRPCService collects grpc service server
type GRPCService struct {
	account.UnimplementedUserServiceServer
	account.UnimplementedAgentServiceServer
	account.UnimplementedAccessServiceServer
}
