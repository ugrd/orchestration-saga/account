package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/interface/validator"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// GetRoleByID is a method to retrieve role by id
func (hdl *Handler) GetRoleByID(ctx context.Context, req *account.IdentifierRequest) (*account.Role, error) {
	v := &validator.IdentifierID{
		ID: req.GetId(),
	}

	if err := v.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(err)).
			ErrList()
	}

	r, err := hdl.Dependency.Repository.Role.FindByID(ctx, uint(req.GetId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return &account.Role{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// GetListRole is a method to retrieve roles
func (hdl *Handler) GetListRole(ctx context.Context, req *account.FilterQuery) (*account.ListRole, error) {
	var roles []*account.Role

	rows, err := hdl.Dependency.Repository.Role.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, r := range rows {
		roles = append(roles, &account.Role{
			Id:          uint64(r.ID),
			Name:        r.Name,
			Code:        r.Code,
			Description: r.Description,
			CreatedAt:   timestamppb.New(r.CreatedAt),
			UpdatedAt:   timestamppb.New(r.UpdatedAt),
		})
	}

	return &account.ListRole{
		Roles: roles,
	}, nil
}
