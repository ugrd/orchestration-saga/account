package handler

import (
	"gitlab.com/ugrd/orchestration-saga/account/interface/handler/core"
	"gitlab.com/ugrd/orchestration-saga/account/interface/handler/dependency"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
)

// Interface is an interface
type Interface interface {
	account.UserServiceServer
	account.AgentServiceServer
	account.AccessServiceServer
}

// Handler is struct
type Handler struct {
	*dependency.Dependency
	*core.GRPCService
}

// New is a constructor
func New(opts ...Option) *Handler {
	handler := &Handler{
		Dependency: &dependency.Dependency{},
	}

	for _, opt := range opts {
		opt(handler)
	}

	return handler
}

var _ Interface = &Handler{}
