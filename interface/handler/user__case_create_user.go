package handler

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/security"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"gitlab.com/ugrd/orchestration-saga/account/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// CreateUser is a method to create new user
func (hdl *Handler) CreateUser(ctx context.Context, req *account.CreateUserRequest) (*account.User, error) {
	payload := &UserCreatePayload{
		Name:     req.GetName(),
		Email:    req.GetEmail(),
		Username: req.GetUsername(),
		Password: req.GetPassword(),
	}

	errV := payload.Validate()
	if errV != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(util.ErrToMap(errV)).
			ErrList()
	}

	if !util.IsTruePass(payload.Password) {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"password": "At least one upper case letter, one lower case latter, one digit, one special character",
			}).
			ErrList()
	}

	_, errEmail := hdl.Dependency.Repository.User.FindByEmail(ctx, req.GetEmail())
	if errEmail == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"email": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	_, errUsername := hdl.Dependency.Repository.User.FindByUsername(ctx, req.GetUsername())
	if errUsername == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"username": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	payload.Password = security.HashMake(payload.Password)

	db := hdl.Dependency.Repository.DB
	tx := db.Begin(&sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	user := entity.User{
		Name:     payload.Name,
		Email:    payload.Email,
		Username: payload.Username,
		Password: payload.Password,
	}

	err := tx.WithContext(ctx).Create(&user).Error
	if err != nil {
		tx.Rollback()
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	userID := user.ID

	_, err = hdl.Dependency.RPC.CreateBalance(ctx, &payment.BalancePaymentRequest{
		UserId:        uint64(userID),
		TransactionId: utils.GenerateUniqueToken(),
		Amount:        0,
	})
	if err != nil {
		tx.Rollback()
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	if errCommit := tx.Commit().Error; errCommit != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ := hdl.Dependency.
		Repository.
		User.FindByID(ctx, userID)

	return &account.User{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Email:     r.Email,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}

// UserCreatePayload is a struct
type UserCreatePayload struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate is a method to validate
func (u UserCreatePayload) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(&u.Email, validation.Required, validation.Length(5, 100), is.Email),
		validation.Field(
			&u.Username,
			validation.Required,
			validation.Length(6, 101),
			validation.Match(regexp.MustCompile("^[A-Za-z][A-Za-z0-9_]{6,29}$")).Error("Must be alpha-numeric, can include underscore"),
		),
		validation.Field(
			&u.Password,
			validation.Required,
			validation.Length(8, 255),
		),
	)
}
