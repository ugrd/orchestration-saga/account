package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListAgent is a method to retrieve agents
func (hdl *Handler) GetListAgent(ctx context.Context, _ *account.FilterQuery) (*account.ListAgent, error) {
	var agents []*account.Agent

	rows, err := hdl.Dependency.Repository.Agent.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, r := range rows {
		agents = append(agents, &account.Agent{
			Id:        uint64(r.ID),
			Name:      r.Name,
			Username:  r.Username,
			CreatedAt: timestamppb.New(r.CreatedAt),
			UpdatedAt: timestamppb.New(r.UpdatedAt),
		})
	}

	return &account.ListAgent{
		Agents: agents,
	}, nil
}
