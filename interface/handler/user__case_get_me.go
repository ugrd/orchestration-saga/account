package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// GetMe is a method to retrieve user by id
func (hdl *Handler) GetMe(ctx context.Context, _ *account.GetMeRequest) (*account.User, error) {
	usname := ctx.Value(entity.XCodeKey)
	if usname == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unavailable).
			WithMsg(fmt.Sprintf("Unauthorized")).
			Err()
	}

	r, err := hdl.Dependency.Repository.User.FindByUsername(ctx, usname.(string))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_, err = hdl.Dependency.RPC.GetBalanceByUserID(ctx, &payment.BalanceIdentifierByUserIDRequest{
		UserId: uint64(r.ID),
	})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &account.User{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Username:  r.Username,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}
