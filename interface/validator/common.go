package validator

import validation "github.com/go-ozzo/ozzo-validation/v4"

// IdentifierID is a common validation
type IdentifierID struct {
	ID uint64 `json:"id"`
}

// Validate is a method to validate
func (i IdentifierID) Validate() error {
	return validation.ValidateStruct(&i,
		validation.Field(&i.ID, validation.Required),
	)
}
