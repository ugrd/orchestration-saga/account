package seeder

import (
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/infrastructure/registry"
)

// Seeder is a struct
type Seeder struct {
	repo *registry.Repositories
}

// NewSeeder is a constructor
func NewSeeder(repo *registry.Repositories) *Seeder {
	return &Seeder{repo: repo}
}

// Seed is a method to run seeders
func (sd *Seeder) Seed(seeds ...Seed) error {
	for _, seeder := range seeds {
		if err := seeder.Seed(sd.repo); err != nil {
			return fmt.Errorf("error seed: %v", err)
		}
	}

	return nil
}
