package seeder

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/infrastructure/registry"
)

// RoleSeeder is a struct
type RoleSeeder struct{}

// Seed is a method
func (rs *RoleSeeder) Seed(repo *registry.Repositories) error {
	roles := []entity.Role{
		{
			Name:        fmt.Sprintf("ADMIN"),
			Code:        fmt.Sprintf("ADMIN"),
			Description: fmt.Sprintf("Is a role for"),
		},
		{
			Name:        fmt.Sprintf("SUPER ADMIN"),
			Code:        fmt.Sprintf("SUPER_ADMIN"),
			Description: fmt.Sprintf("Is a role for"),
		},
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	defer cancel()

	for _, row := range roles {
		r, _ := repo.Role.FindByCode(ctx, row.Code)
		if r != nil {
			continue
		}

		err := repo.Role.Create(ctx, &row)
		for err != nil {
			return err
		}
	}

	return nil
}

var _ Seed = &RoleSeeder{}
