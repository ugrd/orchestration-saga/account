package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/domain/repository"
	"gorm.io/gorm"
)

// UserRepo is a struct
type UserRepo struct {
	db *gorm.DB
}

// NewUserRepo is a constructor
func NewUserRepo(db *gorm.DB) *UserRepo {
	return &UserRepo{db: db}
}

// Create is a method to create new user
func (u UserRepo) Create(ctx context.Context, user *entity.User) error {
	return u.db.WithContext(ctx).Create(user).Error
}

// Update is a method to update existing user
func (u UserRepo) Update(ctx context.Context, user *entity.User, id uint) error {
	return u.db.WithContext(ctx).Where("id = ?", id).Updates(user).Error
}

// FindByID is a method to retrieve user by id
func (u UserRepo) FindByID(ctx context.Context, id uint) (*entity.User, error) {
	var user entity.User

	err := u.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// FindByUsername is a method to retrieve user by username
func (u UserRepo) FindByUsername(ctx context.Context, username string) (*entity.User, error) {
	var user entity.User

	err := u.db.WithContext(ctx).
		Where("username = ?", username).Take(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// FindByEmail is a method to retrieve user by email
func (u UserRepo) FindByEmail(ctx context.Context, email string) (*entity.User, error) {
	var user entity.User

	err := u.db.WithContext(ctx).
		Where("email = ?", email).Take(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// List is a method to retrieve users
func (u UserRepo) List(ctx context.Context) ([]*entity.User, error) {
	var users []*entity.User

	err := u.db.WithContext(ctx).Find(&users).Error
	if err != nil {
		return nil, err
	}

	return users, nil
}

var _ repository.UserRepoInterface = &UserRepo{}
