package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
	"gorm.io/gorm"
)

// RoleRepo is a struct
type RoleRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r RoleRepo) Create(ctx context.Context, role *entity.Role) error {
	return r.db.WithContext(ctx).Create(role).Error
}

// Update is a method to update existing role
func (r RoleRepo) Update(ctx context.Context, role *entity.Role, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(role).Error
}

// Delete is a method to remove existing role
func (r RoleRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Role{}, id).Error
}

// FindByID is a method to retrieve role by id
func (r RoleRepo) FindByID(ctx context.Context, id uint) (*entity.Role, error) {
	var role entity.Role

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&role).Error
	if err != nil {
		return nil, err
	}

	return &role, nil
}

// FindByCode is a method to retrieve role by code
func (r RoleRepo) FindByCode(ctx context.Context, code string) (*entity.Role, error) {
	var role entity.Role

	err := r.db.WithContext(ctx).
		Where("code = ?", code).
		Take(&role).Error
	if err != nil {
		return nil, err
	}

	return &role, nil
}

// FindAll is a method to retrieve roles
func (r RoleRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Role, error) {
	var roles []*entity.Role

	err := r.db.WithContext(ctx).Find(&roles).Error
	if err != nil {
		return nil, err
	}

	return roles, nil
}

// NewRoleRepo is a constructor
func NewRoleRepo(db *gorm.DB) *RoleRepo {
	return &RoleRepo{db: db}
}

var _ repository.RoleRepoInterface = &RoleRepo{}
