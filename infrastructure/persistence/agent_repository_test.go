package persistence_test

import (
	"github.com/jaswdr/faker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/tests"
	"gorm.io/gorm"
	"testing"
)

func TestAgentRepo_Create(t *testing.T) {
	tst := tests.Init()
	ctx := tst.Ctx

	repo := tst.Repo

	roleID := uint(1)
	f := faker.New()

	t.Run("it should return error", func(t *testing.T) {
		err := repo.Agent.Create(ctx, &entity.Agent{
			RoleID:   roleID,
			Name:     f.Person().Name(),
			Username: f.Person().NameMale(),
			Password: f.RandomStringWithLength(10),
		})

		assert.NoError(t, err)
	})
}

func TestAgentRepo_FindByID(t *testing.T) {
	tst := tests.Init()
	ctx := tst.Ctx

	repo := tst.Repo
	f := faker.New()

	roleID := uint(1)
	id := uint(1)

	t.Run("it should return error", func(t *testing.T) {
		r, err := repo.Agent.FindByID(ctx, id)

		assert.Error(t, err)
		assert.Equal(t, gorm.ErrRecordNotFound, err)
		assert.Nil(t, r)
	})

	_ = repo.Agent.Create(ctx, &entity.Agent{
		ID:       id,
		RoleID:   roleID,
		Name:     f.Person().Name(),
		Username: f.Person().NameMale(),
		Password: f.RandomStringWithLength(10),
	})

	t.Run("it should return no error", func(t *testing.T) {
		r, err := repo.Agent.FindByID(ctx, id)

		assert.NoError(t, err)
		assert.NotNil(t, r)
	})
}

func TestAgentRepo_FindByUsername(t *testing.T) {
	tst := tests.Init()
	ctx := tst.Ctx

	repo := tst.Repo
	f := faker.New()

	var (
		roleID   = 1
		id       = 1
		username = "TEST001"
	)

	t.Run("it should return error", func(t *testing.T) {
		r, err := repo.Agent.FindByUsername(ctx, username)

		assert.Error(t, err)
		assert.Equal(t, gorm.ErrRecordNotFound, err)
		assert.Nil(t, r)
	})

	_ = repo.Agent.Create(ctx, &entity.Agent{
		ID:       uint(id),
		RoleID:   uint(roleID),
		Name:     f.Person().Name(),
		Username: username,
		Password: f.RandomStringWithLength(10),
	})

	t.Run("it should return no error", func(t *testing.T) {
		r, err := repo.Agent.FindByUsername(ctx, username)

		assert.NoError(t, err)
		assert.NotNil(t, r)
	})
}
