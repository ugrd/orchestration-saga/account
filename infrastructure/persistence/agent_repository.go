package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/account/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/account/pkg/common"
	"gorm.io/gorm"
)

// AgentRepo is a struct
type AgentRepo struct {
	db *gorm.DB
}

// Create is a method to create new agent
func (ag *AgentRepo) Create(ctx context.Context, agent *entity.Agent) error {
	return ag.db.WithContext(ctx).Create(agent).Error
}

// Update is a method to update existing agent
func (ag *AgentRepo) Update(ctx context.Context, agent *entity.Agent, id uint) error {
	return ag.db.WithContext(ctx).Where("id = ?", id).Updates(agent).Error
}

// Delete is a method to remove existing agent
func (ag *AgentRepo) Delete(ctx context.Context, id uint) error {
	return ag.db.WithContext(ctx).Delete(&entity.Agent{}, id).Error
}

// FindByID is a method to retrieve by id
func (ag *AgentRepo) FindByID(ctx context.Context, id uint) (*entity.Agent, error) {
	var agent entity.Agent

	err := ag.db.WithContext(ctx).
		Joins("Role").
		Where("agents.id = ?", id).
		Take(&agent).Error
	if err != nil {
		return nil, err
	}

	return &agent, nil
}

// FindByUsername is a method to retrieve by username
func (ag *AgentRepo) FindByUsername(ctx context.Context, username string) (*entity.Agent, error) {
	var agent entity.Agent

	err := ag.db.WithContext(ctx).
		Joins("Role").
		Where("agents.username = ?", username).
		Take(&agent).Error
	if err != nil {
		return nil, err
	}

	return &agent, nil
}

// FindAll is a method to retrieve agents
func (ag *AgentRepo) FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Agent, error) {
	var agents []*entity.Agent

	err := ag.db.WithContext(ctx).
		Joins("Role").
		Find(&agents).Error
	if err != nil {
		return nil, err
	}

	return agents, nil
}

// NewAgentRepo is a constructor
func NewAgentRepo(db *gorm.DB) *AgentRepo {
	return &AgentRepo{db: db}
}

var _ repository.AgentRepoInterface = &AgentRepo{}
