package registry

import (
	"gitlab.com/ugrd/orchestration-saga/account/domain/contract"
	"gitlab.com/ugrd/orchestration-saga/account/domain/entity"
)

// CollectEntities is function collects entities
func CollectEntities() []contract.Entity {
	return []contract.Entity{
		{Entity: entity.User{}},
		{Entity: entity.Role{}},
		{Entity: entity.Agent{}},
	}
}

// CollectTables is function collects entity names
func CollectTables() []contract.Table {
	var user entity.User
	var role entity.Role
	var agent entity.Agent

	return []contract.Table{
		{Name: user.TableName()},
		{Name: role.TableName()},
		{Name: agent.TableName()},
	}
}
