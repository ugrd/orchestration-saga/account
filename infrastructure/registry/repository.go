package registry

import (
	"gitlab.com/ugrd/orchestration-saga/account/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/account/infrastructure/persistence"
	"gorm.io/gorm"
)

// Repositories is a struct which collect repositories
type Repositories struct {
	DB    *gorm.DB
	User  repository.UserRepoInterface
	Role  repository.RoleRepoInterface
	Agent repository.AgentRepoInterface
}

// NewRepo is constructor
func NewRepo(db *gorm.DB) *Repositories {
	return &Repositories{
		DB:    db,
		User:  persistence.NewUserRepo(db),
		Role:  persistence.NewRoleRepo(db),
		Agent: persistence.NewAgentRepo(db),
	}
}
