package service

import (
	"gitlab.com/ugrd/orchestration-saga/account/config"
	"gitlab.com/ugrd/orchestration-saga/account/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/account/interface/handler"
	"gitlab.com/ugrd/orchestration-saga/account/interface/interceptor"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// GRPCService is a struct
type GRPCService struct {
	config    *config.Config
	repo      *registry.Repositories
	jwt       *jwt.JWT
	rpcClient *rpc.RPC
}

// NewGRPCService is a constructor
func NewGRPCService(opts ...Option) *GRPCService {
	service := &GRPCService{}
	for _, op := range opts {
		op(service)
	}

	return service
}

// Run is method to run grpc server
func (c *GRPCService) Run(port int) error {
	intercept := interceptor.New(c.config, c.repo, c.jwt)

	server := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			intercept.UnaryLoggerServerInterceptor(),
			intercept.UnaryAuthServerInterceptor(registry.RPCMethods()),
		),
	)

	// instantiate grpc service server handler
	hdl := handler.New(
		handler.WithConfig(c.config),
		handler.WithRepo(c.repo),
		handler.WithJWT(c.jwt),
		handler.WithRPCClient(c.rpcClient),
	)

	account.RegisterUserServiceServer(server, hdl)
	account.RegisterAgentServiceServer(server, hdl)
	account.RegisterAccessServiceServer(server, hdl)

	// register service server to reflection
	reflection.Register(server)

	return utils.RunGRPCServer(server, port)
}

var _ Server = &GRPCService{}
