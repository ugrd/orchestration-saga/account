package service

import (
	"gitlab.com/ugrd/orchestration-saga/account/config"
	"gitlab.com/ugrd/orchestration-saga/account/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc"
	"gitlab.com/ugrd/orchestration-saga/package/security/jwt"
)

// Option is a function
type Option func(*GRPCService)

// WithConfig is a function option
func WithConfig(conf *config.Config) Option {
	return func(service *GRPCService) {
		service.config = conf
	}
}

// WithRepo is a function option
func WithRepo(repo *registry.Repositories) Option {
	return func(service *GRPCService) {
		service.repo = repo
	}
}

// WithJWT is a function option
func WithJWT(j *jwt.JWT) Option {
	return func(service *GRPCService) {
		service.jwt = j
	}
}

// WithRPCClient is a function option
func WithRPCClient(c *rpc.RPC) Option {
	return func(service *GRPCService) {
		service.rpcClient = c
	}
}
