pull_package:
	@go clean -cache
	@go get -v -u gitlab.com/ugrd/orchestration-saga/package@latest

pull_proto:
	@go clean -cache
	@go get -v -u gitlab.com/ugrd/orchestration-saga/proto@latest

run: # run grpc server
	@go run main.go

build: # build go application
	@go build -ldflags="-X 'main.Version=${BUILD_VERSION}'" -v -a -installsuffix -o main .